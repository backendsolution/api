# Blog Management service

A Micro-service component with the capability to create, read, update and delete (CRUD) operations of the post, required to manage a blog.


Swagger/OpenApi doc available in 
http://127.0.0.1:8000/docs


![Alt text](resources/OpenApi.png)