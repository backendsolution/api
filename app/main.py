from typing import Optional
from fastapi import FastAPI, Response, status, HTTPException
from pydantic import BaseModel
from random import randrange

app = FastAPI()

# Model of the post.
class Post(BaseModel):
    title: str
    content: str
    published: bool = True
    rating: Optional[int] = None

# Temporarily store the post in an array. Loses data while server refresh.
my_posts_array = []

# Root path of the application
@app.get("/")
async def root():
    return {"message": "Root path"}

# Retrieve all the post in the memory
@app.get("/posts")
def get_posts():
    return {"data": my_posts_array }

# Save the post into the memory
@app.post("/posts", status_code = status.HTTP_201_CREATED)
def create_post(post: Post):
    new_post=post.model_dump()
    new_post['id'] = randrange(1,10000)
    my_posts_array.append(new_post)
    return{"data": new_post }

# Find the post from the memory
def find_post(id):
    for post in my_posts_array:
        if post['id'] == id:
            return post

# Find the post by id from the memory.    
@app.get("/post/{id}")
def get_post_by_id(id:int, response: Response):
    post = find_post(id)
    if not post:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"post with id {id}, not found")
    return {"data":post}


# Delete the post by id from the memory
@app.delete("/posts/{id}", status_code=status.HTTP_204_NO_CONTENT)
def delete_post(id: int):
    post = find_post(id)
    if post:
        my_posts_array.remove(post)
        return Response(status_code=status.HTTP_204_NO_CONTENT)
    elif not post:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"post with id {id}, does not Exit.")

# Update the existing posts title and content.     
@app.put("/posts/{id}")
def update_posts(id : int, post: Post):
    existing_post = find_post(id) 
    if existing_post :
        unpac = post.model_dump()
        existing_post['title'] = unpac['title']
        existing_post['content'] = unpac['content']
        return {"message": "updated post"}
    elif not existing_post:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"post with id {id}, does not Exit.")          
